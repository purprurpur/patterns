import {PizzaIngredientsFactory} from "./PizzaIngredientFactory";
import {Cheese} from "../Cheese/Cheese";
import {Clams} from "../Clams/Clams";
import {Dough} from "../Dough/Dough";
import {Pepperoni} from "../Pepperoni/Pepperoni";
import {Sauce} from "../Sauces/Sauce";
import {Veggie} from "../Veggies/Veggie";
import {BlackOlives} from "../Veggies/BlackOlives";
import {Spinach} from "../Veggies/Spinach";
import {EggPlant} from "../Veggies/EggPlant";
import {PlumTomatoSauce} from "../Sauces/PlumTomatoSauce";
import {SlicedPepperoni} from "../Pepperoni/SlicedPepperoni";
import {ThickCrustDough} from "../Dough/ThickCrustDough";
import {FrozenClams} from "../Clams/FrozenClams";
import {MozzarellaCheese} from "../Cheese/MozzarellaCheese";

export class ChicagoPizzaIngredientFactory implements PizzaIngredientsFactory {
    createCheese(): Cheese {
        return new MozzarellaCheese();
    }

    createClams(): Clams {
        return new FrozenClams();
    }

    createDough(): Dough {
        return new ThickCrustDough();
    }

    createPepperoni(): Pepperoni {
        return new SlicedPepperoni();
    }

    createSauce(): Sauce {
        return new PlumTomatoSauce();
    }

    createVeggies(): Veggie[] {
        return [new EggPlant(), new Spinach(), new BlackOlives()];
    }
    
}
