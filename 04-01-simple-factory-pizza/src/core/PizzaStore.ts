import {SimplePizzaFabric} from "./SimplePizzaFabric";
import {Pizza} from "./Pizza/Pizza";

export class PizzaStore {
    public constructor(protected fabric: SimplePizzaFabric) {
    }

    public orderPizza(type: string): Pizza {
        let pizza: Pizza = this.fabric.createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }
}
