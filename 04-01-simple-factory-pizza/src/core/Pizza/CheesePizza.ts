import {Pizza} from "./Pizza";

export class CheesePizza extends Pizza {
    public constructor() {
        super();
        this.description = "I'm a  cheeeeezy pizza";
    }
}
