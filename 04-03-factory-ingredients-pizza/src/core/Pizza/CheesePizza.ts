import {Pizza} from "./Pizza";
import {PizzaIngredientsFactory} from "../Ingredients/Factories/PizzaIngredientFactory";

export class CheesePizza extends Pizza {
    public constructor(protected ingredientFactory: PizzaIngredientsFactory) {
        super();
        this.name = "I'm a cheeeeezy pizza";
    }

    public prepare(): void {
        console.log("preparing " + this.name);
        this.dough = this.ingredientFactory.createDough();
        this.cheese = this.ingredientFactory.createCheese();
        this.sauce = this.ingredientFactory.createSauce();
    }
}
