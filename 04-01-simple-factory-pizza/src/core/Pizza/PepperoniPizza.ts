import {Pizza} from "./Pizza";

export class PepperoniPizza extends Pizza {
    public constructor() {
        super();
        this.description = "I'm a pepperoni pizza";
    }
}
