import {Pizza} from "../Pizza/Pizza";
import {PizzaIngredientsFactory} from "../Ingredients/Factories/PizzaIngredientFactory";
import {CheesePizza} from "../Pizza/CheesePizza";
import {PepperoniPizza} from "../Pizza/PepperoniPizza";
import {ClamPizza} from "../Pizza/ClamPizza";

export declare type NullablePizza = Pizza | null;

export abstract class PizzaStore {
    public orderPizza(type: string): Pizza {
        let pizza: Pizza = this.createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }

    protected pizzaSelector(type: string, pFactory: PizzaIngredientsFactory) {
        let pizza: NullablePizza = null;
        if (type === "cheesy") {
            pizza = new CheesePizza(pFactory);
        }

        if (type === "pepperoni") {
            pizza = new PepperoniPizza(pFactory);
        }

        if (type === "clam") {
            pizza = new ClamPizza(pFactory);
        }

        if (!pizza) {
            throw Error("no such pizza");
        }
        return pizza;
    }

    protected abstract createPizza(type: string): Pizza
}
