import {Pizza} from "../Pizza";

export class NYPepperoniPizza extends Pizza {
    public constructor() {
        super();
        this.description = "I'm a NY pepperoni pizza";
    }
}
