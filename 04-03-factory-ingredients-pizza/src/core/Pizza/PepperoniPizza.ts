import {Pizza} from "./Pizza";
import {PizzaIngredientsFactory} from "../Ingredients/Factories/PizzaIngredientFactory";

export class PepperoniPizza extends Pizza {
    public constructor(protected ingredientFactory: PizzaIngredientsFactory) {
        super();
        this.name = "I'm a pepperoni pizza";
    }

    public prepare(): void {
        console.log("Preparing " + this.name);
        this.dough = this.ingredientFactory.createDough();
        this.sauce = this.ingredientFactory.createSauce();
        this.cheese = this.ingredientFactory.createCheese();
        this.pepperoni = this.ingredientFactory.createPepperoni();
    }
}
