import {NullablePizza, PizzaStore} from "./PizzaStore";
import {Pizza} from "../Pizza/Pizza";
import {ChicagoCheesePizza} from "../Pizza/Chicago/ChicagoCheesePizza";
import {ChicagoPepperoniPizza} from "../Pizza/Chicago/ChicagoPepperoniPizza";


export class ChicagoPizzaStore extends PizzaStore {
    protected createPizza(type: string): Pizza {
        let pizza: NullablePizza = null;

        if (type === "cheesy") {
            pizza = new ChicagoCheesePizza();
        }

        if (type === "pepperoni") {
            pizza = new ChicagoPepperoniPizza();
        }
        if (!pizza) {
            throw Error("no such pizza");
        }
        return pizza;
    }
    
}
