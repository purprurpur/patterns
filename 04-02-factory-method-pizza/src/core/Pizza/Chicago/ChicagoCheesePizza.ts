import {Pizza} from "../Pizza";

export class ChicagoCheesePizza extends Pizza {
    public constructor() {
        super();
        this.description = "I'm a Chicago cheeeeezy pizza";
    }
}
