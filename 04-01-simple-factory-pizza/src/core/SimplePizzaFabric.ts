import {CheesePizza} from "./Pizza/CheesePizza";
import {PepperoniPizza} from "./Pizza/PepperoniPizza";
import {Pizza} from "./Pizza/Pizza";

export class SimplePizzaFabric {
    public createPizza(type: string): Pizza {
        let pizza: Pizza;

        if (type === "Cheesy") {
            pizza = new  CheesePizza();
        }
        if (type === "Pepperoni") {
            pizza = new  PepperoniPizza();
        }

        // @ts-ignore
        if (!pizza) {
            throw Error("No such pizza!");
        }

        return pizza;

    }
}
