import {SimplePizzaFabric} from "./core/SimplePizzaFabric";
import {Pizza} from "./core/Pizza/Pizza";
import {PizzaStore} from "./core/PizzaStore";

let pizza: Pizza =  new PizzaStore(new SimplePizzaFabric()).orderPizza("Cheesy");
console.dir({pizza});
