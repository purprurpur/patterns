import {Pizza} from "../Pizza";

export class ChicagoPepperoniPizza extends Pizza {
    public constructor() {
        super();
        this.description = "I'm a Chicago pepperoni pizza";
    }
}
