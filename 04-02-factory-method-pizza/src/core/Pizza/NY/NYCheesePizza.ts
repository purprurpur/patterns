import {Pizza} from "../Pizza";

export class NYCheesePizza extends Pizza {
    public constructor() {
        super();
        this.description = "I'm a NY cheeeeezy pizza";
    }
}
